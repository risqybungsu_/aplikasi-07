package apriliana.risqy.appxx07

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var intentIntegrator : IntentIntegrator
    //// Inisiasi Database, Adapter dan Builder
    lateinit var db : SQLiteDatabase
    lateinit var adapter: ListAdapter
    lateinit var builder: AlertDialog.Builder
    ////

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnScanQR -> {
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQR->{
                val barCodeEncoder = BarcodeEncoder()
                val bitmap = barCodeEncoder.encodeBitmap(edQrCode.text.toString(),
                    BarcodeFormat.QR_CODE, 400,400)
                imV.setImageBitmap(bitmap)
            }
            ///// Tombol Simpan Ke Database
            R.id.btnSimpan -> {
                builder.setTitle("Konfirmasi").setMessage("Simpan ke database?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            /////
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if (intentResult!=null){
            if(intentResult.contents!=null){
                edQrCode.setText(intentResult.contents)
                ///// Untuk Memasukkan Data Agar Terpecah berdasarkan Delimiter
                val strToken = StringTokenizer (edQrCode.text.toString(),";",false)
                edNim.setText(strToken.nextToken())
                edNamaMhs.setText(strToken.nextToken())
                edProdi.setText(strToken.nextToken())
                /////

            }else{
                Toast.makeText(this,"Dibatalkan",Toast.LENGTH_SHORT).show()
            }

        }

        super.onActivityResult(requestCode, resultCode, data)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        intentIntegrator = IntentIntegrator(this)
        btnGenerateQR.setOnClickListener(this)
        btnScanQR.setOnClickListener(this)
        ////// Mengaktifkan Database, Builde, dan Button Simpan
        db = DBOpenHelper(this).writableDatabase
        builder = AlertDialog.Builder(this)
        btnSimpan.setOnClickListener(this)
        /////
    }

    /////

    //////Start
    override fun onStart() {
        super.onStart()
        showDataMhs()
    }
    /////

    ///// Menampilkan data mahasiswa dengan item data
    fun showDataMhs(){
        val cursor : Cursor = db.query("mhs", arrayOf("nim as _id","nama","prodi"),
            null,null,null,null,"nim asc")
        adapter = SimpleCursorAdapter(this,R.layout.item_data_mhs,cursor,
            arrayOf("_id","nama","prodi"), intArrayOf(R.id.txNim,R.id.txNamaMhs,R.id.txProdi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsMhs.adapter = adapter
    }
    /////

    ///// Perintah untuk memanggil insert dan mengirim data yang berasal dari textfield nim, nama, prodi
    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataMhs(
            edNim.text.toString(),
            edNamaMhs.text.toString(),
            edProdi.text.toString()
        )

        edNim.setText("")
        edNamaMhs.setText("")
        edProdi.setText("")
    }
    /////


    ///// Perintah Insert ke Database
    fun insertDataMhs(nim:String, nama:String, alamat:String){
        var cv : ContentValues = ContentValues()
        cv.put("nim",nim)
        cv.put("nama",nama)
        cv.put("prodi",alamat)
        db.insert("mhs",null,cv)
        showDataMhs()
        Toast.makeText(this,"Data Berhasil Ditambahkan",Toast.LENGTH_SHORT).show()
    }

}
